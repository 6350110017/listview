import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    '....',
    'พรี้ลาม',
    'Pto',
    'Smile',
    'Arhhh',
    'Cat Ahhh',
    'Cat Beluga',
    'sad cat',
    'Doge'
  ];

  final images = [
    AssetImage('asset/img/img1.jpg'),
    AssetImage('asset/img/img2.jpg'),
    AssetImage('asset/img/img3.jpg'),
    AssetImage('asset/img/img4.jpg'),
    AssetImage('asset/img/img5.jpg'),
    AssetImage('asset/img/img6.jpg'),
    AssetImage('asset/img/img7.jpg'),
    AssetImage('asset/img/img8.jpg'),
    AssetImage('asset/img/img9.jpg')
  ];

  final subtitle = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('listView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                 title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: const Icon(
                  Icons.notifications_none,
                  size: 25,
                ),
                onTap: (){
                      Fluttertoast.showToast(msg:'${titles[index]},\n ${subtitle[index]}',
                      toastLength: Toast.LENGTH_SHORT,
                  );
                      //print('${titles[index]}');
                },
              ),
              const Divider(
                thickness: 4,
              ),
            ],
          );
        },
      ),
    );
  }
}
