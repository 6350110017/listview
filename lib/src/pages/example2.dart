import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView 2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 50,
            ),
            title: Text('8.00AM',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text('Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Train');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 50,
            ),
            title: Text('8.00AM',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text('Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Bus');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 50,
            ),
            title: Text('8.00AM',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text('Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Bike');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_run,
              size: 50,
            ),
            title: Text('8.00AM',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text('Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Run');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 50,
            ),
            title: Text('8.00AM',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text('Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Car');
            },
          ),
        ],
      ),
    );
  }
}
